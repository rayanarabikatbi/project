import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:project/Features/home/data/repos/home_repo.dart';
import 'package:project/core/utils/injection.dart';
import 'Features/home/presentation/views/home_view.dart';
import 'Features/home/presentation/views_model/comment/comment_bloc.dart';

void main() {
  initGetIt();
  runApp(const ApiApp());
}

class ApiApp extends StatelessWidget {
  const ApiApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: BlocProvider(
        create: (context) => CommentBloc(
          getIt.get<HomeRepo>(),
        )..add(
            CommentFetched(),
          ),
        child: const HomeView(),
      ),
    );
  }
}

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';

import 'package:project/Features/home/data/repos/home_repo_imp.dart';

import '../../../data/models/comment_model.dart';

part 'comment_event.dart';
part 'comment_state.dart';

class CommentBloc extends Bloc<CommentEvent, CommentState> {
  final HomeRepoImp _homeRepoImp;
  CommentBloc(
    this._homeRepoImp,
  ) : super(CommentInitial()) {
    on<CommentFetched>(_onFetched);
    on<CommentCreated>(_onCreateComment);
  }
  void _onFetched(
    CommentFetched event,
    Emitter<CommentState> emit,
  ) async {
    emit(CommentLoading());
    var result = await _homeRepoImp.getComments();
    print('result = $result');
    emit(CommentSuccess(result));
    print('emit suceess');
  }

  void _onCreateComment(
      CommentCreated event, Emitter<CommentState> emit) async {
    emit(CommentLoading());
    var result = await _homeRepoImp.createComment();
    emit(CreateCommentSuccess(result));
    print("emit create success");
  }
}

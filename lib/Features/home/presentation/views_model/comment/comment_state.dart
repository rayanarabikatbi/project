part of 'comment_bloc.dart';

abstract class CommentState extends Equatable {
  const CommentState();

  @override
  List<Object> get props => [];
}

class CommentInitial extends CommentState {}

class CommentLoading extends CommentState {}

class CommentSuccess extends CommentState {
  final List<CommentModel> comments;

  const CommentSuccess(this.comments);
  @override
  List<Object> get props => [comments];
}

class CommentFailure extends CommentState {
  final String errorMsg;

  const CommentFailure(this.errorMsg);
}

class CreateCommentSuccess extends CommentState {
  final CommentModel comment;

const  CreateCommentSuccess(this.comment);
  
}

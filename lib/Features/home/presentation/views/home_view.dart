// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:project/Features/home/presentation/views/add_comment_view.dart';
import 'package:project/Features/home/presentation/views_model/comment/comment_bloc.dart';

class HomeView extends StatelessWidget {
  const HomeView({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        onPressed: () {
        Navigator.of(context).push(
          MaterialPageRoute(
            builder: (context) => AddCommentView(),
          ),
        );
        
      }),
      appBar: AppBar(
        title: Text('Posts'),
        backgroundColor: Colors.orange,
      ),
      body: BlocBuilder<CommentBloc, CommentState>(
        builder: (context, state) {
          if (state is CommentLoading) {
            return Center(
              child: CircularProgressIndicator(),
            );
          }
          if (state is CommentSuccess) {
            return ListView.builder(
                itemCount: state.comments.length,
                itemBuilder: (context, index) {
                  return Card(
                    child: ListTile(
                      title: Text(
                        state.comments[index].name,
                        style: TextStyle(
                          fontSize: 28,
                        ),
                      ),
                      subtitle: Text(state.comments[index].body),
                    ),
                  );
                });
          }
          if (state is CommentFailure) {
            return Container(
              color: Colors.red,
            );
          }
          return Container();
        },
      ),
    );
  }
}

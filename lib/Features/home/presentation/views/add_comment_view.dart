import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:project/Features/home/data/repos/home_repo.dart';
import 'package:project/Features/home/presentation/views_model/comment/comment_bloc.dart';
import 'package:project/core/utils/injection.dart';

class AddCommentView extends StatelessWidget {
  AddCommentView({super.key});

  TextEditingController nameController = TextEditingController();
  TextEditingController emailController = TextEditingController();
  TextEditingController bodyController = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: Column(
        children: [
          const Text('Name'),
          TextField(
              //  controller: nameController,
              ),
          const Text('Email'),
          TextField(
              //  controller: emailController,
              ),
          const Text('Body'),
          TextField(
              //controller: bodyController,
              ),
          ElevatedButton(
            onPressed: () {
              HomeRepo(apiService: getIt()).createComment(
                name: nameController.text,
                email: emailController.text,
                body: bodyController.text,
              );

              print("create comment");

              //  Future.delayed(const Duration(seconds: 2)).then(
              //     (value) =>

              //  getIt.(
              //   name: nameController.text,
              //   email: emailController.text,
              //   body: bodyController.text,
              // ),
              // );
            },
            child: const Text("Submit"),
          ),
        ],
      ),
    );
  }
}

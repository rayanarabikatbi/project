import 'package:dio/dio.dart';
import 'package:project/Features/home/data/repos/home_repo_imp.dart';

import '../../../../core/utils/api_service.dart';
import '../models/comment_model.dart';

class HomeRepo extends HomeRepoImp {
  final ApiService apiService;
  final Dio dio = Dio();
  //final String _baseUrl = "https://jsonplaceholder.typicode.com/posts";
  HomeRepo({
    required this.apiService,
  });

  @override
  Future<List<CommentModel>> getComments() async {
    var response = await apiService.getComments();
    return response;
  }

  @override
  Future<CommentModel> createComment({
    String? name,
    String? email,
    String? body,
  }) async {
    var response = await apiService.createComment();
    return response;
  }
  // @override
  // Future<List<PostModel>> getPosts() async {
  //   var response = await dio.get(_baseUrl);
  //   //  var response = await apiService.get();
  //   // var data = json.decode(response.data);
  //   List responseData = response.data;
  //   // print('data is $data');
  //   return responseData
  //       .map<PostModel>(
  //         (post) => PostModel.fromJson(post),
  //       )
  //       .toList();

  // var response = await dio.get(baseUrl);
  // var responseData = response.data;
  // print("respons= ${response.data}");
  // var jsonData = json.decode(responseData);
  //print("jsonData= $jsonData");
  // return responseData
  //     .map<CommentModel>(
  //       (comment) => CommentModel.fromJson(comment),
  //     )
  //     .toList();
  // }

  // @override
  // Future<PostModel> addPost(
  //   PostModel postModel,
  // ) async {
  //   Response response = await dio.post(
  //     _baseUrl,
  //     data: postModel.toJson(),
  //   );
  //   print("statusCode=${response.statusCode}");
  //   print("response=${response.data}");
  //   return PostModel.fromJson(response.data);
  // }
}

import '../models/comment_model.dart';

abstract class HomeRepoImp {
  Future<List<CommentModel>> getComments();
  Future<CommentModel> createComment();
}

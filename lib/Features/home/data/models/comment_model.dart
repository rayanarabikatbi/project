import 'package:equatable/equatable.dart';

class CommentModel extends Equatable {
  const CommentModel({
    required this.id,
    required this.postId,
    required this.name,
    required this.email,
    required this.body,
  });

  final int id;
  final int postId;
  final String name;
  final String email;
  final String body;

  factory CommentModel.fromJson(Map<String, dynamic> json) {
    return CommentModel(
      id: json["id"] ?? 0,
      postId: json["post_id"] ?? 0,
      name: json["name"] ?? "",
      email: json["email"] ?? "",
      body: json["body"] ?? "",
    );
  }

  Map<String, dynamic> toJson() => {
        "id": id,
        "post_id": postId,
        "name": name,
        "email": email,
        "body": body,
      };

  @override
  List<Object?> get props => [
        id,
        postId,
        name,
        email,
        body,
      ];
}


import 'package:flutter/material.dart';

import '../../../../core/widgets/custom_textformfield.dart';

class AddPostView extends StatelessWidget {
  AddPostView({super.key});
  final TextEditingController titleController = TextEditingController();
  final TextEditingController descriptionController = TextEditingController();
 // final HomeRepoImp homeRepo = HomeRepoImp();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Add Post'),
        backgroundColor: Colors.orange,
      ),
      body: Form(
        child: Padding(
          padding: const EdgeInsets.all(16),
          child:
              Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
            const Text('Title'),
            const SizedBox(height: 5),
            CustomTextField(
              hintText: 'add title post',
              controller: titleController,
            ),
            const SizedBox(height: 10),
            const Text('Subtitle Post'),
            const SizedBox(height: 5),
            CustomTextField(
              hintText: 'add description for  post',
              maxLine: null,
              controller: descriptionController,
            ),
            ElevatedButton(
              onPressed: () {
                // homeRepo.addPost(PostModel(
                //   title: titleController.text,
                //   body: descriptionController.text,
                // ));
                print('success');
              },
              child: const Text(
                'Add Post',
              ),
            )
          ]),
        ),
      ),
    );
  }
}

import 'dart:convert';

import 'package:dio/dio.dart';

import '../../Features/home/data/models/comment_model.dart';

String baseUrl = "https://gorest.co.in/public/v2/comments";

class ApiService {
  final Dio dio;

  ApiService(this.dio);
  Future<List<CommentModel>> getComments() async {
    // const String baseUrl = "https://gorest.co.in/public/v2/comments";
    var response = await dio.get("$baseUrl");
    List responseData = response.data;
    print("respons= ${response.data}");
    return responseData
        .map(
          (comment) => CommentModel.fromJson(comment),
        )
        .toList();
  }

  Future<CommentModel> createComment(
    {
    String? name,
    String? email,
    String? body,
 }
  ) async {
    String token =
        "da1ae588c0c72409735e26f9e4d9c7ef8e4a74c6589fc13da57b72775d98b2cf";

    var response = await dio.post(
      "https://gorest.co.in/public/v2/posts/586/comments",
      data: {
        "id": 55,
        "post_id": 56,
        "name": name,
        "email": email,
        "body":body,
      },
      options: Options(headers: {
        "Content-Type": "application/json",
        "Authorization": "Bearer $token",
      }),
    );
    print("response POST=${response.data.toString()}");
    print(response.statusCode);
    return CommentModel.fromJson(response.data);
  }
}
//   Future<dynamic> get() async {
//     var response = await dio.get(_baseUrl);
//     print('response is ${response.data}');
//     return response;
//   }
// }
//  Response response = await dio.get(_baseUrl);
//     print("response is $response");

//     return (response.data as List)
//         .map(
//           (e) => PostModel.fromJson(e),
//         )
//         .toList();
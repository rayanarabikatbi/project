

import 'package:dio/dio.dart';
import 'package:get_it/get_it.dart';
import 'package:project/Features/home/data/repos/home_repo.dart';
import 'package:project/Features/home/presentation/views_model/comment/comment_bloc.dart';
import 'package:project/core/utils/api_service.dart';


final getIt = GetIt.instance;

void initGetIt(){
  
  getIt.registerLazySingleton<CommentBloc>(() => CommentBloc(getIt()));
  getIt.registerLazySingleton<HomeRepo>(() => HomeRepo(apiService: getIt()));
  getIt.registerLazySingleton<ApiService>(() => ApiService(
        createAndSetupDio(),
      ));
}


Dio createAndSetupDio() {
  Dio dio = Dio();
  dio
    ..options.connectTimeout = const Duration(seconds: 10 * 1000)
    ..options.receiveTimeout = const Duration(seconds: 10 * 1000);
  dio.interceptors.add(
    LogInterceptor(
      responseBody: true,
      error: true,
      requestHeader: false,
      responseHeader: false,
      request: true,
      requestBody: true,
    ),
  );
  return dio;
}
